import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// route-level code splitting
import { createConverterView } from '../views/ConverterView.js'

export function createRouter () {
  return new Router({
    mode: 'history',
    fallback: false,
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      { path: '/converter', component: createConverterView() },
      { path: '/', redirect: '/converter' }
    ]
  })
}
