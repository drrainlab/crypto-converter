import Vue from 'vue'

export default {
  SET_ASSETS: (state, assets) => {
    assets.forEach(asset => {
      if (asset) {
        Vue.set(state.assets, asset, { 
          CoinName: asset
        })
      }
    })
  }
}
