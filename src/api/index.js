import axios from 'axios'

const api_hostname = "https://rest.coinapi.io"
const api_assets_path = "/v1/assets"
const api_rate_path= "/v1/exchangerate/"
const api_headers = {
  // 'Access-Control-Allow-Origin': '*'
  // 'X-CoinAPI-Key': ' 102DC8A1-6EEE-4559-8FBA-1F2EC2631FAD'
}

var api = axios.create({
  // baseURL: api_hostname,
  timeout: 3000
});

for (let prop of Object.keys(api_headers)) 
  api.defaults.headers.common[prop]=api_headers[prop]

  // {
  //   method: 'GET',
  //   mode: 'no-cors',
  //   headers: {
  //     'Access-Control-Allow-Origin': '*',
  //     'Content-Type': 'application/json',
  //   },
  //   withCredentials: true,
  //   credentials: 'same-origin'}

export function fetchAllAssets(){
  return axios("http://coincap.io/coins/")
    .then(result=>result.data)
} 

export function fetchRate(baseID, quoteID) {
  return axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${baseID}&tsyms=${quoteID}`)
    .then(result=>result.data[quoteID] || 0)
}

export function fetchHistoricalData(baseID, quoteID, limit=10) {
  return axios.get(`https://min-api.cryptocompare.com/data/histoday?fsym=${baseID}&tsym=${quoteID}&limit=${limit}`)
    .then(result=>result.data || [])
}


// const https = require('https');

// var options = {
//   "method": "GET",
//   "hostname": "rest.coinapi.io",
//   "path": "/v1/assets",
//   "headers": {'X-CoinAPI-Key': '73034021-0EBC-493D-8A00-E0F138111F41'}
// };

// var request = https.request(options, function (response) {
//   var chunks = [];
//   response.on("data", function (chunk) {
//     chunks.push(chunk);
//   });
// });

// request.end();

// import { createAPI } from 'create-api'

// const logRequests = !!process.env.DEBUG_API

// const api = createAPI({
//   version: '/v0',
//   config: {
//     databaseURL: 'https://hacker-news.firebaseio.com'
//   }
// })

// // warm the front page cache every 15 min
// // make sure to do this only once across all requests
// if (api.onServer) {
//   warmCache()
// }

// function warmCache () {
//   fetchItems((api.cachedIds.top || []).slice(0, 30))
//   setTimeout(warmCache, 1000 * 60 * 15)
// }

// function fetch (child) {
//   logRequests && console.log(`fetching ${child}...`)
//   const cache = api.cachedItems
//   if (cache && cache.has(child)) {
//     logRequests && console.log(`cache hit for ${child}.`)
//     return Promise.resolve(cache.get(child))
//   } else {
//     return new Promise((resolve, reject) => {
//       api.child(child).once('value', snapshot => {
//         const val = snapshot.val()
//         // mark the timestamp when this item is cached
//         if (val) val.__lastUpdated = Date.now()
//         cache && cache.set(child, val)
//         logRequests && console.log(`fetched ${child}.`)
//         resolve(val)
//       }, reject)
//     })
//   }
// }

// export function fetchIdsByType (type) {
//   return api.cachedIds && api.cachedIds[type]
//     ? Promise.resolve(api.cachedIds[type])
//     : fetch(`${type}stories`)
// }

// export function fetchItem (id) {
//   return fetch(`item/${id}`)
// }

// export function fetchItems (ids) {
//   return Promise.all(ids.map(id => fetchItem(id)))
// }

// export function fetchUser (id) {
//   return fetch(`user/${id}`)
// }

// export function watchList (type, cb) {
//   let first = true
//   const ref = api.child(`${type}stories`)
//   const handler = snapshot => {
//     if (first) {
//       first = false
//     } else {
//       cb(snapshot.val())
//     }
//   }
//   ref.on('value', handler)
//   return () => {
//     ref.off('value', handler)
//   }
// }
