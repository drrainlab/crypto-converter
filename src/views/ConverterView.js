import ConverterView from './Converter.vue'

const camelize = str => str.charAt(0).toUpperCase() + str.slice(1)

export function createConverterView() {
  return {
    name: `converter-view`,

    asyncData ({ store }) {
      return store.dispatch('FETCH_ALL_ASSETS')
    },

    title: camelize('Converter'),

    render (h) {
      return h(ConverterView)
    }
  }
}
