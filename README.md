## Project Description

High interactive Crypto Currency Converter

Use [space, enter] keys for show/hide currencies

**Tip: In amount mode start typing first letters of required currency after you entered an amount**

## Build Setup

**Requires Node.js 7+**

``` bash
# install dependencies
npm install # or yarn

# serve in dev mode, with hot reload at localhost:8080
npm run dev

# build for production
npm run build

# serve in production mode
npm start
```

## License

MIT
